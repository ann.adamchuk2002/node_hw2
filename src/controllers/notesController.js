const express = require('express');
const router = express.Router();

const {
    getNotesByUserId,
    addNoteToUser,
    getNoteById,
    deleteNoteById,
    checkNoteById,
    putNoteById
} = require('../services/notesService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const limit = parseInt(req.query.limit) || 0;
    const offset = parseInt(req.query.offset) || 0;
    const notes = await getNotesByUserId(userId, limit, offset);
    res.json({ 
        "offset": Number(limit) + Number(offset),
        "limit": limit,
        "count": notes.count,
        "notes": notes.notes
    })
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;


    const note = await getNoteById(id, userId)

    if (!note) {
        throw new InvalidRequestError('No Note with such id found!');
    }

    res.json({note});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const userId = req.user.userId;
    await addNoteToUser(userId, req.body.text);

    res.json({message: "Note created successfully"});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;
    await checkNoteById(id, userId)

    res.json({message: "Note checked successfully"});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;
    await deleteNoteById(id, userId)

    res.json({message: "Note deleted successfully"});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const userId = req.user.userId;
    const {id} = req.params;
    const text = req.body.text;
  
    await putNoteById(id, userId, text);
  
    res.json({message: 'Note updated successfully'});
  }));

  
module.exports = {
    notesRouter: router
}